﻿using System;
using System.Collections.Generic;

namespace VacuumAgent
{
    public class BreadthFirstShortestPath<T>
    {
        private int _nodesCount;
        private bool[] _visited;
        private long[] _distances;
        private int[] _predecessors;

        private Dictionary<T, int> _nodesToIndices;
        private Dictionary<int, T> _indicesToNodes;

        private const long Inf = long.MaxValue;

        public BreadthFirstShortestPath(DirectedWeightedGraph<T> graph, T source)
        {
            if (graph == null || !graph.HasNode(source))
                throw new ArgumentException("Graph is null or source node is not part of the graph.");

            Initialize(graph);
            BreadthFirstSearch(graph, source);
        }

        private void Initialize(DirectedWeightedGraph<T> graph)
        {
            _nodesCount = graph.NodesCount;

            _visited = new bool[_nodesCount];
            _distances = new long[_nodesCount];
            _predecessors = new int[_nodesCount];
            
            _nodesToIndices = new Dictionary<T, int>();
            _indicesToNodes = new Dictionary<int, T>();
            
            // Initialize arrays

            var i = 0;
            foreach (var node in graph.Nodes)
            {
                if (i >= _nodesCount)
                    break;

                _visited[i] = false;
                _distances[i] = Inf;
                _predecessors[i] = -1;
                
                _nodesToIndices.Add(node, i);
                _indicesToNodes.Add(i, node);

                i++;
            }
        }
        
        /// <summary>
        /// Visit the graph, breadth first.
        /// </summary>
        /// <param name="graph">The graph to be searched.</param>
        /// <param name="source">Entry node.</param>
        private void BreadthFirstSearch(DirectedWeightedGraph<T> graph, T source)
        {
            // Start from source
            _distances[_nodesToIndices[source]] = 0;
            _visited[_nodesToIndices[source]] = true;
                        
            var queue = new Queue<T>();
            queue.Enqueue(source);

            while (queue.Count > 0)
            {
                var current = queue.Dequeue();
                var indexOfCurrent = _nodesToIndices[current];
                
                // Loop through all neighbors of the cuurent node
                foreach (var adjacent in graph.NeighborsMap(current))
                {
                    // Key is the node and Value is the weight
                    var indexOfAdjacent = _nodesToIndices[adjacent.Key];

                    if (!_visited[indexOfAdjacent])
                    {
                        _predecessors[indexOfAdjacent] = indexOfCurrent;
                        _distances[indexOfAdjacent] = _distances[indexOfCurrent] + adjacent.Value;
                        _visited[indexOfAdjacent] = true;
                        
                        // Add each unvisited neighbor to the list of to-be-visited nodes
                        queue.Enqueue(adjacent.Key);
                    }
                }
            }
        }

        /// <summary>
        /// Check if a path is present in the graph from source to destination.
        /// </summary>
        /// <param name="destination">Destination node.</param>
        /// <returns>True if a path exists, false otherwise.</returns>
        /// <exception cref="ArgumentException">Occur when the graph does not have the input node.</exception>
        public bool HasPathTo(T destination)
        {
            if (!_nodesToIndices.ContainsKey(destination))
                throw new ArgumentException("Graph does not have the input node.");

            return _visited[_nodesToIndices[destination]];
        }

        /// <summary>
        /// Compute the distance from the source node to destination.
        /// </summary>
        /// <param name="destination">Destination node.</param>
        /// <returns>The distance from source to destination.</returns>
        /// <exception cref="ArgumentException">Occur when the graph does not have the input node.</exception>
        public long DistanceTo(T destination)
        {
            if (!_nodesToIndices.ContainsKey(destination))
                throw new ArgumentException("Graph does not have the input node.");

            return _distances[_nodesToIndices[destination]];
        }

        /// <summary>
        /// Compute the shortest path from the source node to destination.
        /// </summary>
        /// <param name="destination">Destination node.</param>
        /// <returns>The path from source to destination</returns>
        /// <exception cref="ArgumentException">Occur when the graph does not have the input node.</exception>
        public IEnumerable<T> ShortestPathTo(T destination)
        {
            if (!_nodesToIndices.ContainsKey(destination))
                throw new ArgumentException("Graph does not have the input node.");
            
            // No path available
            if (!HasPathTo(destination))
                return null;
            
            var destIndex = _nodesToIndices[destination];
            var stack = new Stack<T>();
            int index;
            
            // Go back up from destination to the source node
            for (index = destIndex; _distances[index] != 0; index = _predecessors[index])
                stack.Push(_indicesToNodes[index]);
                        
            // Push source node
            stack.Push(_indicesToNodes[index]);

            return stack;
        }
    }
}