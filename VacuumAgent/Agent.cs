﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;

namespace VacuumAgent
{
    public class Agent
    {
        private readonly Environment _env;
        private readonly Belief _belief;
        private readonly bool _informedExploration;
        
        private Room[,] _desire;
        private Queue<Effectors> _nextIntentions;
        private DirectedWeightedGraph<Room> _graph;
        private int _maxActions = 10;
               
        public Agent(Environment env, bool informedExploration)
        {
            _env = env;
            _informedExploration = informedExploration;
            
            _belief = new Belief();
            _desire = new Room[Environment.Size, Environment.Size];

            // The desire of the agent is a mansion well-cleaned
            for (var i = 0; i < Environment.Size; i++)
            {
                for (var j = 0; j < Environment.Size; j++)
                {
                    _desire[i, j] = new Room(i, j);
                }
            }
            
            // Put vacuum cleaner at the start of the mansion
            _belief.UpdateVacuumPosition(0, 0);
        }
        
        /// <summary>
        /// The agent's loop.
        /// </summary>
        public void Loop()
        {
            while (AmIAlive())
            {
                ObserveEnvironment();
                UpdateState();
                ChooseAction();
                ExecuteAction();
                
                Thread.Sleep(500);
            }
        }
        
        /// <summary>
        /// Check if the agent is still alive.
        /// </summary>
        /// <returns>True if the game is still running, false otherwise.</returns>
        private bool AmIAlive()
        {
            return _env.GameIsRunning();
        }

        /// <summary>
        /// Observe environment through sensors.
        /// </summary>
        private void ObserveEnvironment()
        {
            _belief.UpdateDirtyRooms(_env.ObserveDirtyRooms());
            _belief.UpdateLostJewelsRooms(_env.ObserveLostJewels());
        }

        /// <summary>
        /// Update inner state thanks to a previous observation.
        /// </summary>
        private void UpdateState()
        {
            foreach (Point p in _belief.RememberDirtyRooms())
            {
                _belief.Rooms[p.X, p.Y].PutDirt();
            }

            foreach (Point p in _belief.RemembertLostJewelsRooms())
            {
                _belief.Rooms[p.X, p.Y].PutJewel();
            }
            
            // Not sure about the values here
            if (_env.MeasureActualPerformance() > _belief.RememberPerformance())
                _maxActions -= 2;
            else if (_env.MeasureActualPerformance() < _belief.RememberPerformance())
                _maxActions += 3;

            if (_maxActions <= 0)
                _maxActions = 1;
            
            _belief.UpdatePerformance(_env.MeasureActualPerformance());
            _graph = BuildGraphBasedOnBelief();
            
            Console.WriteLine($"Perf is {_belief.RememberPerformance()} and max actions is {_maxActions}");
        }

        /// <summary>
        /// Choose an action, based on the inner state.
        /// </summary>
        private void ChooseAction()
        {
            _nextIntentions = ComputeNextIntentions();
        }

        /// <summary>
        /// Execute the chosen action.
        /// </summary>
        private void ExecuteAction()
        {
            var forceRecomputation = false;
            var i = 0;

            while (_nextIntentions.Count > 0 && !forceRecomputation)
            {
                var nextIntention = _nextIntentions.Dequeue();

                switch (nextIntention)
                {
                    case Effectors.Clean:
                        Clean();
                        break;
                    case Effectors.PickupJewel:
                        PickupJewel();
                        break;
                    case Effectors.MoveUp:
                        MoveUp();
                        break;
                    case Effectors.MoveDown:
                        MoveDown();
                        break;
                    case Effectors.MoveLeft:
                        MoveLeft();
                        break;
                    case Effectors.MoveRight:
                        MoveRight();
                        break;
                }

                if (i > _maxActions)
                    forceRecomputation = true;

                i++;
                
                Thread.Sleep(500);
                Console.WriteLine(nextIntention);
            }
            
        }

        /// <summary>
        /// Build graph of the mansion, based on agent's belief.
        /// </summary>
        /// <returns>A directed weighted graph of the rooms.</returns>
        private DirectedWeightedGraph<Room> BuildGraphBasedOnBelief()
        {
            var graph = new DirectedWeightedGraph<Room>();
            IList<Room> rooms = _belief.Rooms.Cast<Room>().ToList();
            
            graph.AddNodes(rooms);

            for (var x = 0; x < Environment.Size; x++)
            {
                for (var y = 0; y < Environment.Size; y++)
                {
                    var currentRoom = _belief.Rooms[x, y];
                    
                    var upX = x + 1;
                    var downX = x - 1;
                    var upY = y + 1;
                    var downY = y - 1;

                    if (upX < Environment.Size)
                        graph.AddEdge(currentRoom, _belief.Rooms[upX, y], 
                            Environment.ElectricityPerformanceCost);

                    if (upY < Environment.Size)
                        graph.AddEdge(currentRoom, _belief.Rooms[x, upY], 
                            Environment.ElectricityPerformanceCost);

                    if (downX >= 0)
                        graph.AddEdge(currentRoom, _belief.Rooms[downX, y],
                            Environment.ElectricityPerformanceCost);

                    if (downY >= 0)
                        graph.AddEdge(currentRoom, _belief.Rooms[x, downY],
                            Environment.ElectricityPerformanceCost);

                    if (currentRoom.HasDirt())
                    {
                        var proper = new Room(x, y);
                        proper.PutDirt();

                        graph.AddNode(proper);
                        graph.AddEdge(currentRoom, proper, -1 * Environment.CleanedRoomPerformanceBoost);
                    }

                    if (currentRoom.HasJewel())
                    {
                        var proper = new Room(x, y);
                        proper.PutJewel();

                        graph.AddNode(proper);
                        graph.AddEdge(currentRoom, proper, -1 * Environment.CleanedRoomPerformanceBoost);
                    }
                }
            }

            return graph;
        }

        /// <summary>
        /// Compute the next actions to be executed by the vacuum agent.
        /// </summary>
        /// <returns>A Queue of Effectors.</returns>
        public Queue<Effectors> ComputeNextIntentions()
        {
            var queue = new Queue<Effectors>();

            // Check belief
            var pos = _belief.RememberVacuumPosition();
            var dirtyRooms = _belief.RememberDirtyRooms();
            var jewelsLost = _belief.RemembertLostJewelsRooms();
            
            // Check which room worth cleaning is the closest
            var bfs = new BreadthFirstShortestPath<Room>(_graph, _belief.Rooms[pos.X, pos.Y]);
            var aStar = new AStarShortestPath<Room>(_graph, _belief.Rooms[pos.X, pos.Y]);
            var closest = FindClosestRoomWorthCleaning(bfs, dirtyRooms, jewelsLost);

            // The mansion is clean!
            if (closest == null)
                return queue;
            
            // Path to this room
            Stack<Room> stack;
            
            if (_informedExploration)
                stack = (Stack<Room>) aStar.ShortestPathTo(_belief.Rooms[closest.Position.X, closest.Position.Y], HeuristicCostEstimate);
            else
                stack = (Stack<Room>) bfs.ShortestPathTo(_belief.Rooms[closest.Position.X, closest.Position.Y]);
            
            // Remove the first step (where he already is)
            var lastRoom = stack.Pop();
            
            // Pop every item in the stack and plan the intentions
            while (stack.Count > 0)
            {
                var roomInPath = stack.Pop();
                var xDiff = roomInPath.Position.X - pos.X;
                var yDiff = roomInPath.Position.Y - pos.Y;

                if (xDiff == 1)
                {
                    queue.Enqueue(Effectors.MoveRight);
                    pos.X += 1;
                }
                else if (xDiff == -1)
                {
                    queue.Enqueue(Effectors.MoveLeft);
                    pos.X -= 1;
                }
                else if (yDiff == 1)
                {
                    queue.Enqueue(Effectors.MoveDown);
                    pos.Y += 1;
                }
                else if (yDiff == -1)
                {
                    queue.Enqueue(Effectors.MoveUp);
                    pos.Y -= 1;
                }

                if (stack.Count <= 0)
                    lastRoom = roomInPath;
            }
            
            // Clean the room
            if (lastRoom != null && lastRoom.HasJewel())
                queue.Enqueue(Effectors.PickupJewel);
            
            if (lastRoom != null && lastRoom.HasDirt())
                queue.Enqueue(Effectors.Clean);
            
            return queue;
        }

        /// <summary>
        /// Find the closest room worth cleaning (where dirt or a jewel is present).
        /// </summary>
        /// <param name="bfs">Instance of BFS for the graph.</param>
        /// <param name="dirtyRooms">An ArrayList of the dirty rooms' positions.></param>
        /// <param name="jewelsLost">An ArrayList of the lost jewels' positions.</param>
        /// <returns></returns>
        private Room FindClosestRoomWorthCleaning(
            BreadthFirstShortestPath<Room> bfs, ArrayList dirtyRooms, ArrayList jewelsLost)
        {
            var rooms = new Dictionary<long, Room>();
            var roomsDistances = new List<long>();
            
            // Convert ArrayList of Points to List of Rooms
            var toBeCleanedRooms = (from Point dirty in dirtyRooms select _belief.Rooms[dirty.X, dirty.Y]).ToList();
            toBeCleanedRooms.AddRange(from Point jewels in jewelsLost select _belief.Rooms[jewels.X, jewels.Y]);

            // Add rooms' distances
            foreach (var room in toBeCleanedRooms)
            {
                var distance = bfs.DistanceTo(room);

                // Ignore if one room with the same distance already exists, choose the one which is already added
                if (!rooms.ContainsKey(distance))
                    rooms.Add(distance, room);
                
                roomsDistances.Add(distance);
            }
            
            if (roomsDistances.Count <= 0)
                return null;
            
            // Sort and return closest room worth cleaning
            roomsDistances.Sort();

            return rooms[roomsDistances[0]];
        }

        /// <summary>
        /// Update vacuum's position in inner state.
        /// </summary>
        /// <param name="x">X position of the vacuum cleaner.</param>
        /// <param name="y">Y position of the vacuum cleaner.</param>
        private void UpdateVacuumPosition(int x, int y)
        {
            if (x >= Environment.Size || y >= Environment.Size || x < 0 || y < 0)
                return;

            _belief.UpdateVacuumPosition(x, y);
        }

        /// <summary>
        /// Heuristic estimation of the cost to go from source to dest.
        /// </summary>
        /// <param name="source">Source room.</param>
        /// <param name="dest">Destination room.</param>
        /// <returns>The estimated cost.</returns>
        private long HeuristicCostEstimate(Room source, Room dest)
        {
            long realCost = dest.Position.X - source.Position.X + dest.Position.Y - source.Position.Y;
            long heuristic = (long) Math.Sqrt(
                Math.Pow(dest.Position.X - source.Position.X, 2) + Math.Pow(dest.Position.Y - source.Position.Y, 2));

            return realCost + heuristic;
        }
        
        /*
         * Agent's actions.
         */

        private void MoveUp()
        {
            var vacuum = _belief.RememberVacuumPosition();
            UpdateVacuumPosition(vacuum.X, vacuum.Y - 1);
            
            _env.ExecuteVacuumIntention(Effectors.MoveUp);
        }

        private void MoveDown()
        {
            var vacuum = _belief.RememberVacuumPosition();
            UpdateVacuumPosition(vacuum.X, vacuum.Y + 1);

            _env.ExecuteVacuumIntention(Effectors.MoveDown);
        }


        private void MoveLeft()
        {
            var vacuum = _belief.RememberVacuumPosition();
            UpdateVacuumPosition(vacuum.X - 1, vacuum.Y);

            _env.ExecuteVacuumIntention(Effectors.MoveLeft);
        }

        private void MoveRight()
        {
            var vacuum = _belief.RememberVacuumPosition();
            UpdateVacuumPosition(vacuum.X + 1, vacuum.Y);

            _env.ExecuteVacuumIntention(Effectors.MoveRight);
        }

        private void PickupJewel()
        {
            var vacuum = _belief.RememberVacuumPosition();
            
            _belief.Rooms[vacuum.X, vacuum.Y].RemoveJewel();
            _env.ExecuteVacuumIntention(Effectors.PickupJewel);
        }

        private void Clean()
        {
            var vacuum = _belief.RememberVacuumPosition();
            
            _belief.Rooms[vacuum.X, vacuum.Y].RemoveDirt();
            _env.ExecuteVacuumIntention(Effectors.Clean);
        }
    }
}