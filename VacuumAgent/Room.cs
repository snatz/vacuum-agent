﻿using System.Drawing;

namespace VacuumAgent
{
    public class Room
    {
        private Point _pos;
        private bool _hasJewel;
        private bool _hasDirt;
        private bool _hasVacuumCleaner;

        public Room(int x, int y)
        {
            _pos = new Point(x, y);
        }

        public Room(Point p)
        {
            _pos = p;
        }
        
        public Point Position => _pos;

        public void RemoveDirt()
        {
            _hasDirt = false;
        }

        public void RemoveJewel()
        {
            _hasJewel = false;
        }

        public void RemoveVacuumCleaner()
        {
            _hasVacuumCleaner = false;
        }

        public void PutDirt()
        {
            _hasDirt = true;
        }

        public void PutJewel()
        {
            _hasJewel = true;
        }

        public void PutVacuumCleaner()
        {
            _hasVacuumCleaner = true;
        }

        public bool HasDirt()
        {
            return _hasDirt;
        }

        public bool HasJewel()
        {
            return _hasJewel;
        }

        public bool HasVacuumCleaner()
        {
            return _hasVacuumCleaner;
        }
        
        public override string ToString()
        {
            return $"Room ({_pos.X},{_pos.Y}). Jewel: {_hasJewel}. Dirt: {_hasDirt}. Vacuum: {_hasVacuumCleaner}.";
        }
    }
}