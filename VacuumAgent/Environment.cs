﻿using System;
using System.Collections;
using System.Drawing;
using System.Threading;

namespace VacuumAgent
{
    public class Environment
    {
        private bool _formClosed;
        private GraphicalInterface _form;
        private Point _vacuumPosition;
        private int _performance;  // Performance to be maximized
        
        public const int Size = 10;
        private const int DirtySpaceProbability = 25;
        private const int LostJewelProbability = 10;
        
        // Performance units/costs
        public const int CleanedRoomPerformanceBoost = 10;
        public const int AspiratedJewelPerformanceBoost = -20;
        public const int ElectricityPerformanceCost = 1;
        public const int VacuumActionPerformanceCost = 1;
        
        private readonly Room[,] _map = new Room[Size, Size];
        private readonly Random _rand = new Random();

        public Environment(GraphicalInterface form)
        {
            _form = form;
            _vacuumPosition = new Point(0, 0);
            _performance = 0;
            
            // Observer on the GUI closing event
            _form.FormClosing += (sender, e) =>
            {
                _formClosed = true;
            };

            // Initialize the map
            for (var i = 0; i < Size; i++)
            {
                for (var j = 0; j < Size; j++)
                {
                    _map[i, j] = new Room(i, j);
                }
            }
            
            // Put vacuum cleaner at the start of the mansion
            _map[0, 0].PutVacuumCleaner();
            _form.AddVacuumTo(0, 0);
        }
        
        /// <summary>
        /// The environment's loop.
        /// </summary>
        public void Loop()
        {
            while (GameIsRunning())
            {
                if (ShouldThereBeANewDirtySpace())
                    GenerateDirt();

                if (ShouldThereBeANewLostJewel())
                    GenerateJewel();
                
                Thread.Sleep(1000);
            }
        }
        
        /*
         * Public functions (agent sensors)
         */

        /// <summary>
        /// Check if the game is still running.
        /// </summary>
        /// <returns>True if the game is running (GUI still opened), false otherwise.</returns>
        public bool GameIsRunning()
        {
            return !_formClosed;
        }

        /// <summary>
        /// Observe the dirty rooms (rooms with dirt in it).
        /// </summary>
        /// <returns>An ArrayList containing the positions of the dirty rooms.</returns>
        public ArrayList ObserveDirtyRooms()
        {
            var dirtyList = new ArrayList();
            
            for (var i = 0; i < Size; i++)
            {
                for (var j = 0; j < Size; j++)
                {
                    if (_map[i, j].HasDirt())
                    {
                        dirtyList.Add(new Point(i, j));
                    }
                }
            }

            return dirtyList;
        }

        /// <summary>
        /// Observe in which rooms the lost jewels are located.
        /// </summary>
        /// <returns>An ArrayList containing the positions of the rooms where jewels are lost.</returns>
        public ArrayList ObserveLostJewels()
        {
            var jewelsList = new ArrayList();

            for (var i = 0; i < Size; i++)
            {
                for (var j = 0; j < Size; j++)
                {
                    if (_map[i, j].HasJewel())
                    {
                        jewelsList.Add(new Point(i, j));
                    }
                }
            }

            return jewelsList;
        }

        /// <summary>
        /// Move the vacuum cleaner according to his action.
        /// </summary>
        /// <param name="effector">The action chosen by the vacuum.</param>
        public void ExecuteVacuumIntention(Effectors effector)
        {
            if (effector == Effectors.MoveDown || effector == Effectors.MoveLeft ||
                effector == Effectors.MoveUp || effector == Effectors.MoveRight)
            {
                _map[_vacuumPosition.X, _vacuumPosition.Y].RemoveVacuumCleaner();
                _form.RemoveVacuumFrom(_vacuumPosition.X, _vacuumPosition.Y);
            }

            switch (effector)
            {
                case Effectors.MoveDown:
                    if (_vacuumPosition.Y < Size - 1)
                        _vacuumPosition.Y += 1;
                    break;
                case Effectors.MoveUp:
                    if (_vacuumPosition.Y > 0)
                        _vacuumPosition.Y -= 1;
                    break;
                case Effectors.MoveRight:
                    if (_vacuumPosition.X < Size - 1)
                        _vacuumPosition.X += 1;
                    break;
                case Effectors.MoveLeft:
                    if (_vacuumPosition.X > 0)
                        _vacuumPosition.X -= 1;
                    break;
                case Effectors.PickupJewel:
                    PickupJewel();
                    break;
                case Effectors.Clean:
                    Clean();
                    break;
                default:
                    return;
            }

            if (effector == Effectors.MoveDown || effector == Effectors.MoveLeft ||
                effector == Effectors.MoveUp || effector == Effectors.MoveRight)
            {
                _map[_vacuumPosition.X, _vacuumPosition.Y].PutVacuumCleaner();
                _form.AddVacuumTo(_vacuumPosition.X, _vacuumPosition.Y);
                _performance -= ElectricityPerformanceCost;
            }
        }

        /// <summary>
        /// Measure the actual performance.
        /// </summary>
        /// <returns>The vacuum's performance.</returns>
        public int MeasureActualPerformance()
        {
            return _performance;
        }

        /// <summary>
        /// The vacuum picks up a jewel if present in the room.
        /// </summary>
        private void PickupJewel()
        {
            if (_map[_vacuumPosition.X, _vacuumPosition.Y].HasJewel())
                _performance += CleanedRoomPerformanceBoost;
            
            _map[_vacuumPosition.X, _vacuumPosition.Y].RemoveJewel();
            _form.RemoveJewelFrom(_vacuumPosition.X, _vacuumPosition.Y);
            _performance -= VacuumActionPerformanceCost;
        }
        
        /// <summary>
        /// The vacuum cleans the room it is in at the moment.
        /// </summary>
        private void Clean()
        {
            if (_map[_vacuumPosition.X, _vacuumPosition.Y].HasDirt())
                _performance += CleanedRoomPerformanceBoost;

            if (_map[_vacuumPosition.X, _vacuumPosition.Y].HasJewel())
                _performance += AspiratedJewelPerformanceBoost;
            
            _map[_vacuumPosition.X, _vacuumPosition.Y].RemoveDirt();
            _form.RemoveDirtFrom(_vacuumPosition.X, _vacuumPosition.Y);
            _performance -= VacuumActionPerformanceCost;
        }
        
        /*
         * Environment generation functions
         */
        
        /// <summary>
        /// Randomly select if it should spawns a new dirty space.
        /// </summary>
        /// <returns>True if it should spawns one, false otherwise.</returns>
        private bool ShouldThereBeANewDirtySpace()
        {
            return GenerateRandomBoolWithProbability(DirtySpaceProbability);
        }

        /// <summary>
        /// Randomly select if it should spanws a new lost jewel.
        /// </summary>
        /// <returns>True if it should spawns one, false otherwise.</returns>
        private bool ShouldThereBeANewLostJewel()
        {
            return GenerateRandomBoolWithProbability(LostJewelProbability);
        }

        /// <summary>
        /// Generate a new lost jewel in a random room on the map.
        /// </summary>
        private void GenerateJewel()
        {
            int x, y;

            // Find a room without any jewel
            do
            {
                x = _rand.Next(Size);
                y = _rand.Next(Size);
            } while (_map[x, y].HasJewel());

            _map[x, y].PutJewel();
            _form.AddJewelTo(x, y);
            
            Console.WriteLine($"Generated a jewel on ({x},{y})");
        }

        /// <summary>
        /// Generate a new dirty space in a random room on the map.
        /// </summary>
        private void GenerateDirt()
        {
            int x, y;

            // Find a room without any dirt
            do
            {
                x = _rand.Next(Size);
                y = _rand.Next(Size);
            } while (_map[x, y].HasDirt());

            _map[x, y].PutDirt();
            _form.AddDirtTo(x, y);
            
            Console.WriteLine($"Generated a dirty space on ({x},{y})");
        }

        /// <summary>
        /// Generate a random boolean with a certain probability to be true.
        /// </summary>
        /// <param name="prob">The true probability.</param>
        /// <returns>True or false.</returns>
        private bool GenerateRandomBoolWithProbability(int prob)
        {
            return _rand.Next(100) < prob;
        }
    }
}