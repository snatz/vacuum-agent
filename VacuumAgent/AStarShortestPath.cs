﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VacuumAgent
{
    public class AStarShortestPath<T>
    {
        private int _nodesCount;
        private DirectedWeightedGraph<T> _graph;
        
        private List<T> _closedSet;  // The nodes already evaluated
        private List<T> _openSet;  // The nodes discovered yet to be evaluated
        private Dictionary<T, T> _cameFrom;  // For each node, which node it can most efficiently be reached from.
        
        private Dictionary<int, long> _distancesFromSource;  // The distance/cost of getting from start node to a node
        private Dictionary<int, long> _distancesPassingByNode;  // The cost of getting from start to goal, passing by a node
        private Dictionary<T, int> _nodesToIndices;  // A mapping node/index

        private readonly T _source;

        private const long Inf = long.MaxValue;
        
        public AStarShortestPath(DirectedWeightedGraph<T> graph, T source)
        {
            if (graph == null || !graph.HasNode(source))
                throw new ArgumentException("Graph is null or does not have the source node.");

            _source = source;
            _graph = graph;
            
            Intialize(graph);           
        }
        
        /// <summary>
        /// Find the shortest path from source to the goal, using a specific heuristic function.
        /// </summary>
        /// <param name="goal">Destination node.</param>
        /// <param name="heuristicCostEstimate">Heuristic function which returns a long integer.</param>
        /// <returns>The shortest path.</returns>
        public IEnumerable<T> ShortestPathTo(T goal, Func<T, T, long> heuristicCostEstimate)
        {
            _distancesPassingByNode[_nodesToIndices[_source]] = heuristicCostEstimate(_source, goal);

            while (_openSet.Count > 0)
            {
                var current = FindLowestDistancePassingByNode();

                if (current.Equals(goal))
                    return ReconstructPath(current);
                
                _openSet.Remove(current);
                _closedSet.Add(current);

                foreach (var neighbor in _graph.NeighborsMap(current))
                {
                    if (_closedSet.Contains(neighbor.Key))
                        continue;  // Ignore the neighbor already evaluated
                    
                    if (!_openSet.Contains(neighbor.Key))
                        _openSet.Insert(_nodesToIndices[neighbor.Key], neighbor.Key);

                    // The distance from start to a neighbor
                    var tentativeDistance = _distancesFromSource[_nodesToIndices[current]] + 
                                             _graph.GetEdgeWeight(current, neighbor.Key);

                    if (tentativeDistance >= _distancesFromSource[_nodesToIndices[neighbor.Key]])
                        continue;  // Not a better path

                    // The best path yet
                    _cameFrom[neighbor.Key] = current;
                    _distancesFromSource[_nodesToIndices[neighbor.Key]] = tentativeDistance;
                    _distancesPassingByNode[_nodesToIndices[neighbor.Key]] =
                        _distancesFromSource[_nodesToIndices[neighbor.Key]] + heuristicCostEstimate(neighbor.Key, goal);
                }
            }

            return null;
        }

        /// <summary>
        /// Reconstruct the best path backwards thanks to the _cameFrom dictionary.
        /// </summary>
        /// <param name="current">Goal node.</param>
        /// <returns>The reconstructed path.</returns>
        private IEnumerable<T> ReconstructPath(T current)
        {
            var path = new Stack<T>();
            
            path.Push(current);

            while (_cameFrom.Keys.Contains(current))
            {
                current = _cameFrom[current];
                path.Push(current);
            }
            
            return path;
        }

        /// <summary>
        /// Find the node in the path source node to goal, and in the open set,
        /// which have the lowest distance of the said path.
        /// </summary>
        /// <returns>The node which have the lowest distance.</returns>
        private T FindLowestDistancePassingByNode()
        {
            var min = Inf;
            var minNode = default(T);

            foreach (var entry in _openSet)
            {
                if (entry == null) continue;
                
                if (_distancesPassingByNode[_nodesToIndices[entry]] < min)
                {
                    min = _distancesPassingByNode[_nodesToIndices[entry]];
                    minNode = entry;
                }
            }
            
            return minNode;
        }

        private void Intialize(DirectedWeightedGraph<T> graph)
        {
            _nodesCount = graph.NodesCount;
            
            _closedSet = new List<T>();
            _openSet = new List<T>(new T[100]);
            _cameFrom = new Dictionary<T, T>();
            
            _distancesFromSource = new Dictionary<int, long>();
            _distancesPassingByNode = new Dictionary<int, long>();
            
            _nodesToIndices = new Dictionary<T, int>();
            
            // Initialize fields

            var i = 0;
            foreach (var node in graph.Nodes)
            {
                if (i >= _nodesCount)
                    break;

                _nodesToIndices.Add(node, i);

                _distancesFromSource[i] = Inf;

                i++;
            }
            
            _openSet.Insert(_nodesToIndices[_source], _source);
            _distancesFromSource[_nodesToIndices[_source]] = 0; 
        }
    }
}