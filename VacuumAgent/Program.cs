﻿using System.Threading;
using System.Windows.Forms;

namespace VacuumAgent
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var informedExploration = args.Length > 0 && args[0].Equals("--informed");
            
            var gui = new GraphicalInterface();
            var env = new Environment(gui);
            var agent = new Agent(env, informedExploration);
            
            var environmentThread = new Thread(env.Loop);
            var agentThread = new Thread(agent.Loop);
            
            environmentThread.Start();
            agentThread.Start();
            Application.Run(gui);
        }
    }
}