﻿using System;
using System.Collections;
using System.Drawing;

namespace VacuumAgent
{
    public class Belief
    {
        public Room[,] Rooms;
        
        private Point _vacuumCleanerPosition;
        private ArrayList _dirtyRooms;
        private ArrayList _lostJewelsRooms;
        private int _performance;
        
        public Belief()
        {
            Rooms = new Room[Environment.Size, Environment.Size];
            _vacuumCleanerPosition = new Point(0, 0);
            
            _dirtyRooms = new ArrayList();
            _lostJewelsRooms = new ArrayList();
            
            // Initialize inner state
            for (var i = 0; i < Environment.Size; i++)
            {
                for (var j = 0; j < Environment.Size; j++)
                {
                    Rooms[i ,j] = new Room(i, j);
                }
            }
            
            Rooms[0, 0].PutVacuumCleaner();
        }

        /// <summary>
        /// Update belief about the dirty rooms.
        /// </summary>
        /// <param name="dirtyRooms">An ArrayList containing the dirty rooms' positions (as Point instances).</param>
        public void UpdateDirtyRooms(ArrayList dirtyRooms)
        {
            _dirtyRooms = dirtyRooms;
        }

        /// <summary>
        /// Update belief about the rooms containing a jewel.
        /// </summary>
        /// <param name="lostJewels">An ArrayList containing the jewels' positions (as Point instances).</param>
        public void UpdateLostJewelsRooms(ArrayList lostJewels)
        {
            _lostJewelsRooms = lostJewels;
        }

        /// <summary>
        /// Update belief about the performance's measure
        /// </summary>
        /// <param name="perf">An integer with the performance's value.</param>
        public void UpdatePerformance(int perf)
        {
            _performance = perf;
        }

        /// <summary>
        /// Update belief about the vacuum's position.
        /// </summary>
        /// <param name="x">The vacuum's x position.</param>
        /// <param name="y">The vacuum's y position.</param>
        public void UpdateVacuumPosition(int x, int y)
        {
            if (x >= Environment.Size || x < 0 || y >= Environment.Size || y < 0)
                return;
            
            _vacuumCleanerPosition.X = x;
            _vacuumCleanerPosition.Y = y;
        }

        /// <summary>
        /// Remember where the dirty rooms are.
        /// </summary>
        /// <returns>The dirty rooms' positions as an ArrayList of Point instances.</returns>
        public ArrayList RememberDirtyRooms()
        {
            return _dirtyRooms;
        }

        /// <summary>
        /// Remember where the lost jewels are.
        /// </summary>
        /// <returns>The lost jewels' positions as an ArrayList of Point instances.</returns>
        public ArrayList RemembertLostJewelsRooms()
        {
            return _lostJewelsRooms;
        }

        /// <summary>
        /// Remember where the vacuum cleaner is.
        /// </summary>
        /// <returns>The vacuum cleaner's position as a Point instance.</returns>
        public Point RememberVacuumPosition()
        {
            return _vacuumCleanerPosition;
        }

        /// <summary>
        /// Remember how much the performance's measure is.
        /// </summary>
        /// <returns>The performance's value.</returns>
        public int RememberPerformance()
        {
            return _performance;
        }
    }
}