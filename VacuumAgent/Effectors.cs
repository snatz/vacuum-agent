﻿namespace VacuumAgent
{
    /// <summary>
    /// Effectors available to the vacuum cleaner agent.
    /// </summary>
    public enum Effectors
    {
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,
        PickupJewel,
        Clean
    }
}
