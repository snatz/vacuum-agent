﻿using System.Drawing;
using System.Windows.Forms;

namespace VacuumAgent
{
    public class GraphicalInterface : Form
    {
        private Label[,] _labels;

        public GraphicalInterface()
        {
            const int gridSize = 10;
            const int tileSize = 60;
            var lightGray = Color.LightGray;
            var white = Color.White;

            Text = "Vacuum agent";
            _labels = new Label[gridSize, gridSize];
            Size = new Size(607, 627);
            
            // Loop through the rooms
            for (var n = 0; n < gridSize; n++)
            {
                for (var m = 0; m < gridSize; m++)
                {
                    // Create a new Label control which will be one tile
                    var newLabel = new Label
                    {
                        Size = new Size(tileSize, tileSize),
                        Location = new Point(tileSize * n, tileSize * m),
                        Text = "",
                        TextAlign = ContentAlignment.MiddleCenter,
                    };
                    
                    newLabel.Font = new Font(newLabel.Font, FontStyle.Bold);
                    
                    // Add to Form's Controls so that they show up
                    Controls.Add(newLabel);
                    
                    // Add to our 2d array of labels for future use
                    _labels[n, m] = newLabel;

                    // Color the labels' backgrounds
                    if (n % 2 == 0)
                        newLabel.BackColor = m % 2 != 0 ? lightGray : white;
                    else
                        newLabel.BackColor = m % 2 != 0 ? white : lightGray;
                }
            }
        }

        /// <summary>
        /// Add dirt to a room on the GUI.
        /// </summary>
        /// <param name="x">Position X of the room.</param>
        /// <param name="y">Position Y of the room.</param>
        public void AddDirtTo(int x, int y)
        {
            _labels[x, y].Text += "\nDirt";
        }
        
        /// <summary>
        /// Add a jewel to a room on the GUI.
        /// </summary>
        /// <param name="x">Position X of the room.</param>
        /// <param name="y">Position Y of the room.</param>
        public void AddJewelTo(int x, int y)
        {
            _labels[x, y].Text += "\nJewel";
        }

        /// <summary>
        /// Add the vacuum cleaner to a room on the GUI.
        /// </summary>
        /// <param name="x">Position X of the room.</param>
        /// <param name="y">Position Y of the room.</param>
        public void AddVacuumTo(int x, int y)
        {
            _labels[x, y].Text = "Vacuum\n" + _labels[x, y].Text;
        }

        /// <summary>
        /// Remove dirt from a room on the GUI.
        /// </summary>
        /// <param name="x">Position X of the room.</param>
        /// <param name="y">Position Y of the room.</param>
        public void RemoveDirtFrom(int x, int y)
        {
            _labels[x, y].Text = _labels[x, y].Text.Replace("\nDirt", "");
        }

        /// <summary>
        /// Remove a jewel from a room on the GUI.
        /// </summary>
        /// <param name="x">Position X of the room.</param>
        /// <param name="y">Position Y of the room.</param>
        public void RemoveJewelFrom(int x, int y)
        {
            _labels[x, y].Text = _labels[x, y].Text.Replace("\nJewel", "");
        }

        /// <summary>
        /// Remove the vacuum cleaner from a room on the GUI.
        /// </summary>
        /// <param name="x">Position X of the room.</param>
        /// <param name="y">Position Y of the room.</param>
        public void RemoveVacuumFrom(int x, int y)
        {
            _labels[x, y].Text =_labels[x, y].Text.Replace("Vacuum\n", "");
        }
    }
}